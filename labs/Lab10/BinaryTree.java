class BinaryTree {

    private class Node {
        protected Node parent, leftChild, rightChild;
        protected int data;

        public Node() {
            parent = null;
            leftChild = null;
            rightChild = null;
            data = -1;
        } //Node (constructor)

        public Node(Node p, Node l, Node r, int d) {
            parent = p;
            leftChild = l;
            rightChild = r;
            data = d;
        } //Node (constructor)

    } //Node (inner class)

    private Node root;
    private int size;;

    private int height;

    public BinaryTree() {
        root = null;
        size = 0;
        height = 0;
    } //BinaryTree (constructor)

    public void insert(int dataToInsert) {

        // add a root
        if (root == null) {
            root = new Node(null, null, null, dataToInsert);
            size++;
            height++;
        } else {
            Node parent = new Node();

            Node current = root;
            while (current != null){
                if (dataToInsert < current.data){
                    parent = current;
                    current = current.leftChild;
                } else if (dataToInsert > current.data) {
                    parent = current;
                    current = current.rightChild;
                }
            } //while

            Node currentNode = new Node(parent, null, null, dataToInsert);
            if (parent.data < dataToInsert){
                parent.rightChild = currentNode;
            } else if (parent.data > dataToInsert){
                parent.leftChild = currentNode;
            } // else-if
            size++;
            height++;
        } //if-else

    } //insert

    public int get(int searchFor) {
        if(root.data == searchFor){
            return root.data;
        } else {

            int q = 0;
            Node current = root;
            Node parent = current;
            while (current.data != searchFor && q != 1){

                if (searchFor > current.data && searchFor < parent.data){
                    q = 1;
                } else if (searchFor < current.data){
                    parent = current;
                    current = current.leftChild;

                } else if (searchFor > current.data) {
                    parent = current;
                    current = current.rightChild;

                } // else-if
            } //while

            if (current.data == searchFor){
                return current.data;
            } else{
                return -1;
            }
        } // if-else
    } //get

    public void remove(int removeValue) {
        Node current = root;
        Node parent = new Node();

        //Iterate through
        while (current.data != removeValue && (current.leftChild != null || current.rightChild != null)){
            if (removeValue < current.data && current.leftChild != null){
                parent = current;
                current = current.leftChild;
            } else if (removeValue > current.data && current.rightChild != null) {
                parent = current;
                current = current.rightChild;
            }
        } //while

        // No children
        if (current.rightChild == null && current.leftChild == null){
            if (current == parent.rightChild){
                parent.rightChild = null;
            } else if (current == parent.leftChild){
                parent.leftChild = null;
            }

            // One child
        } else if ((current.rightChild == null && current.leftChild != null) || (current.rightChild != null && current.leftChild == null)){
            if (current == parent.rightChild){
                if (current.rightChild == null){
                    current = current.leftChild;
                    current.parent = parent;

                    parent.rightChild = current;
                }
                else if (current.leftChild == null){
                    current = current.rightChild;
                    current.parent = parent;

                    parent.rightChild = current;
                } // if-else
            } else if (current == parent.leftChild){
                if (current.rightChild == null){
                    current = current.leftChild;
                    current.parent = parent;

                    parent.leftChild = current;
                }
                else if (current.leftChild == null){
                    current = current.rightChild;
                    current.parent = parent;

                    parent.leftChild = current;
                } // if-else

            } // if-else

        } // if-else

    } //remove

} //BinaryTree (class)
