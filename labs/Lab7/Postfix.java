import java.util.Stack;

public class Postfix {
    public static void main(String[] args) {
        char input[] = args[0].toCharArray();

        if (matcher(input)) {
            System.out.println(toPostfix(input));
        } else {
            System.out.println("Sorry, you didn't give me valid infix notation.");
        }
    } // main

    public static boolean matcher(char[] c) {

        Stack<Character> myStack = new Stack<Character>();

        for (int i = 0; i < c.length; i++) {

            if (c[i] == '(') {
                myStack.push(c[i]);

            } else if (!myStack.empty()) {

                if (c[i] == ')') {
                    char temp = myStack.pop();
                    if (temp != '(') {
                        return false;
                    } //if


                } else if (myStack.empty() && c[i] == ')') {
                    return false;

                }

            } //if-else

        } //for
        return myStack.empty();

    } //matcher

    public static String toPostfix(char[] c) {
        String myString = "";

        Stack<Character> myStack = new Stack<Character>();

        for(int i = 0; i < c.length; i++) {
            if (i == (c.length-1)) {
                if (c[i] != '(' && c[i] != ')' && c[i] != '+' && c[i] != '-' && c[i] != '*' && c[i] != '/' && c[i] != '^') {
                    myString += c[i];
                }
                while (!myStack.empty()) {
                    if (myStack.peek() == '(') {
                        myStack.pop();
                    }
                        char temp = myStack.pop();
                        myString += temp;
                }

            } else if (c[i] != '(' && c[i] != ')' && c[i] != '+' && c[i] != '-' && c[i] != '*' && c[i] != '/' && c[i] != '^') {
                myString += c[i];
            } else if (i == (c.length-1)) {
                    while (!myStack.empty()) {
                        char temp = myStack.pop();
                        myString += temp;
                    }
                    myString += c[i];
            } else if (c[i] == '*' || c[i] == '/') {
                if (myStack.empty()) {
                    myStack.push(c[i]);
                }
                else if (myStack.peek() == '+' || myStack.peek() == '-' ) {
                    myStack.push(c[i]);
                }
                else if (myStack.peek() == '*' || myStack.peek() == '/') {
                    char temp = myStack.pop();
                    myStack.push(c[i]);
                    myString += temp;
                }
                else if (myStack.peek() == '(') {
                    myStack.push(c[i]);
                }
                else if (myStack.peek() == '^') {
                    char temp = myStack.pop();
                    myStack.push(c[i]);
                    myString += temp;
                }

            } else if (c[i] == '+' || c[i] == '-') {
                if (myStack.empty()) {
                    myStack.push(c[i]);
                }
                else if (myStack.peek() == '+' || myStack.peek() == '-' ) {
                    char temp = myStack.pop();
                    myStack.push(c[i]);
                    myString += temp;
                }
                else if (myStack.peek() == '*' || myStack.peek() == '/') {
                    char temp = myStack.pop();
                    myStack.push(c[i]);
                    myString += temp;
                }
                else if (myStack.peek() == '(') {
                    myStack.push(c[i]);
                }
                else if (myStack.peek() == '^') {
                    char temp = myStack.pop();
                    myStack.push(c[i]);
                    myString += temp;
                }
            } else if (c[i] == '^') {
                if (myStack.empty()) {
                    myStack.push(c[i]);
                }
                else if (myStack.peek() == '+' || myStack.peek() == '-' ) {
                    myStack.push(c[i]);
                }
                else if (myStack.peek() == '*' || myStack.peek() == '/') {
                    myStack.push(c[i]);
                }
                else if (myStack.peek() == '(') {
                    myStack.push(c[i]);
                }
                else if (myStack.peek() == '^') {
                    char temp = myStack.pop();
                    myStack.push(c[i]);
                    myString += temp;
                }

            } else if (c[i] == '(') {
                myStack.push(c[i]);
            } else if (c[i] == ')') {
                if (myStack.peek() == '(') {
                    myStack.pop();
                }
                else {
                    while (myStack.peek() != '('){
                        char temp = myStack.pop();
                        myString += temp;
                    }
                    myStack.pop();
                }

            }
        }
    return myString;
} // toPostfix

} // Postfix (class)
