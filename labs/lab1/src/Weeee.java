public class Weeee {

    public static void weeee() {
		System.out.println("Weeee!"); //creates a method to print the word "Weeee!"
		weeee(); //Executes weeee() within weeee()
    } //weeee

    public static void main(String[] args) {
        weeee(); //Executes weeee()
    } //main

} //Weeee (class)
