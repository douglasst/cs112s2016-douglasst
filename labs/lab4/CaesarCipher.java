public class CaesarCipher{
    // private static String code = new String("STOP RIGHT THERE CRIMINAL SCUM");

    public static void main(String[] args){
        String code = new String("STOP RIGHT THERE CRIMINAL SCUM");
        System.out.println("Original Message: \n" + code);
        code = encrypt(code);
        System.out.println("Encrypted Message: \n" + code);
        code = decrypt(code);
        System.out.println("Decrypted Message: \n" + code);
    } // main

    public static String encrypt(String s){
        int len = s.length(); //Must have a variable for length so length stays static
        for (int i = 0; i< len; i++){ //Iterate through string
            char c = s.charAt(i); // Allows for use of ASCII values
            if ( c <= 83 && c >= 65 ){
                s += (char)(s.charAt(i) + 7);
            }
            else if ( c > 83 && c<=90 ){
                s += (char)(s.charAt(i) - 19);
            }
            else{ // To make sure it doesn't encrypt spaces
                s += (char)(s.charAt(i));
            }
        }
        return s.substring(30); //If you don't specify the substring after 30, it will add on
                                // the original PLUS the encryption.
    } // encrypt

    public static String decrypt(String s){
        int len = s.length();
        for (int i = 0; i< len; i++){
            char c = s.charAt(i);
            if ( c <= 90 && c >= 72 ){ //Changing the values to their original ASCII ones
                s += (char)(s.charAt(i) - 7);
            }
            else if ( c >= 65 && c < 72 ){
                s += (char)(s.charAt(i) + 19);
            }
            else{
                s += (char)(s.charAt(i));
            }
        }
        return s.substring(30);

    } // decrypt

} // CaesarCipher (class)
