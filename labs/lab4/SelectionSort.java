public class SelectionSort{
    public static void main(String[] args){
        int butts[] = {7, 33, 14, 2, 9, 8, 15, 42};
        int currentMax;

        for (int i = 0; i < butts.length; i++){ //Will iterate through "sorted" lines of the array

            for (int j = 0; j < butts.length; j++){ //Will iterate through a line to sort it

                if (butts[i] < butts [j]){ // Will switch around values so that the list will sort itself
                    currentMax = butts[i];
                    butts[i] = butts[j];
                    butts[j] = currentMax;
                } // if
                System.out.print(butts[j] + " "); // Prints out the edited row

            } // for
            System.out.println(); // Starts a new row
        } // for

    } // main
} // SelectionSort (class)
