import java.util.Stack;

public class Calculator {
    public static void main(String[] args) {
        char input[] = args[0].toCharArray();

        if (matcher(input)) {
            System.out.println(toPostfix(input));
        } else {
            System.out.println("Sorry, you didn't give me valid infix notation.");
        }

        String postfix = toPostfix(input);
        System.out.println("Solution: " + solve(postfix));
    } // main

    public static boolean matcher(char[] c) {

        Stack<Character> myStack = new Stack<Character>();

        for (int i = 0; i < c.length; i++) {

            if (c[i] == '(') {
                myStack.push(c[i]);

            } else if (!myStack.empty()) {

                if (c[i] == ')') {
                    char temp = myStack.pop();
                    if (temp != '(') {
                        return false;
                    } //if


                } else if (myStack.empty() && c[i] == ')') {
                    return false;

                }

            } //if-else

        } //for
        return myStack.empty();

    } //matcher

    public static String toPostfix(char[] c) {
        String myString = "";

        Stack<Character> myStack = new Stack<Character>();

        for(int i = 0; i < c.length; i++) {
            if (i == (c.length-1)) {
                if (c[i] != '(' && c[i] != ')' && c[i] != '+' && c[i] != '-' && c[i] != '*' && c[i] != '/' && c[i] != '^') {
                    myString += c[i];
                }
                while (!myStack.empty()) {
                    if (myStack.peek() == '(') {
                        myStack.pop();
                    }
                        char temp = myStack.pop();
                        myString += temp;
                }

            } else if (c[i] != '(' && c[i] != ')' && c[i] != '+' && c[i] != '-' && c[i] != '*' && c[i] != '/' && c[i] != '^') {
                myString += c[i];
            } else if (i == (c.length-1)) {
                    while (!myStack.empty()) {
                        char temp = myStack.pop();
                        myString += temp;
                    }
                    myString += c[i];
            } else if (c[i] == '*' || c[i] == '/') {
                if (myStack.empty()) {
                    myStack.push(c[i]);
                }
                else if (myStack.peek() == '+' || myStack.peek() == '-' ) {
                    myStack.push(c[i]);
                }
                else if (myStack.peek() == '*' || myStack.peek() == '/') {
                    char temp = myStack.pop();
                    myStack.push(c[i]);
                    myString += temp;
                }
                else if (myStack.peek() == '(') {
                    myStack.push(c[i]);
                }
                else if (myStack.peek() == '^') {
                    char temp = myStack.pop();
                    myStack.push(c[i]);
                    myString += temp;
                }

            } else if (c[i] == '+' || c[i] == '-') {
                if (myStack.empty()) {
                    myStack.push(c[i]);
                }
                else if (myStack.peek() == '+' || myStack.peek() == '-' ) {
                    char temp = myStack.pop();
                    myStack.push(c[i]);
                    myString += temp;
                }
                else if (myStack.peek() == '*' || myStack.peek() == '/') {
                    char temp = myStack.pop();
                    myStack.push(c[i]);
                    myString += temp;
                }
                else if (myStack.peek() == '(') {
                    myStack.push(c[i]);
                }
                else if (myStack.peek() == '^') {
                    char temp = myStack.pop();
                    myStack.push(c[i]);
                    myString += temp;
                }
            } else if (c[i] == '^') {
                if (myStack.empty()) {
                    myStack.push(c[i]);
                }
                else if (myStack.peek() == '+' || myStack.peek() == '-' ) {
                    myStack.push(c[i]);
                }
                else if (myStack.peek() == '*' || myStack.peek() == '/') {
                    myStack.push(c[i]);
                }
                else if (myStack.peek() == '(') {
                    myStack.push(c[i]);
                }
                else if (myStack.peek() == '^') {
                    char temp = myStack.pop();
                    myStack.push(c[i]);
                    myString += temp;
                }

            } else if (c[i] == '(') {
                myStack.push(c[i]);
            } else if (c[i] == ')') {
                if (myStack.peek() == '(') {
                    myStack.pop();
                }
                else {
                    while (myStack.peek() != '('){
                        char temp = myStack.pop();
                        myString += temp;
                    }
                    myStack.pop();
                }

            }
        }
    return myString;
} // toPostfix

public static int solve(String c){

    Stack<Integer> myStack = new Stack<Integer>();

    for (int i = 0; i<c.length(); i++) {
        if (c.charAt(i) != '+' && c.charAt(i) != '-' && c.charAt(i) != '*' && c.charAt(i) != '/' && c.charAt(i) != '^') {
            int x = Integer.parseInt(Character.toString(c.charAt(i)));
            myStack.push(x);
        } else {
            int b = myStack.pop();
            int a = myStack.pop();
            if (c.charAt(i) == '+'){
                int y = a + b;
                myStack.push(y);
            } else if (c.charAt(i) == '-'){
                int y = a - b;
                myStack.push(y);
            } else if (c.charAt(i) == '*'){
                 int y = a * b;
                 myStack.push(y);
            } else if (c.charAt(i) == '/'){
                int y = a / b;
                myStack.push(y);
            } else if (c.charAt(i) == '^'){
                int y = (int)Math.pow(a,b);
                myStack.push(y);
            } // else-if
        } // else-if

    } // for

    return myStack.pop();
} // solve

} // Postfix (class)
