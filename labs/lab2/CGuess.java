import java.util.Scanner;

public class CGuess{
    public static void main(String[] args){
        int hi, lo, ans, answer, tries, playAgain;
        String playChoice;

        Scanner input = new Scanner(System.in);

        playAgain = 0; //Initializing variables
        answer = 0;
        tries = 0;

        while (playAgain <1){ //Allows for replayability
            if (playAgain == 0){ //If the user chooses to play again, the value will not be incremented

                hi = 100; //Resets the variables for new rounds
                lo = 1;
                ans = 0;

                System.out.println("Think of an integer between 1-100, and I will guess it.");
                System.out.println("If my guess is too high, enter 1. If it's too low, enter -1. If it's correct, enter 0.");
                tries++;

                ans = (hi+lo)/2; // The equation for the computer's guess
                System.out.println("Is your number " +ans+ "?");
                answer = input.nextInt();

                while (answer != 0){ // While loop to run through game after first user input
                    if (answer == 1){

                        hi = ans-1; //This allows the program to reset the max value
                        tries++;
                        ans = (hi+lo)/2;

                        System.out.println("I guess that was too high. Is it " +ans+ "?");
                        answer = input.nextInt();
                    } //if
                    else{

                        lo = ans+1; //Allows the program to reset the min value
                        tries++;
                        ans = (hi+lo)/2;

                        System.out.println("You're right, that's too low. How about " +ans+ "?");
                        answer = input.nextInt();
                    } //else
                } //while
                System.out.println("Ah! So your answer is " +ans+ "! It took me " +tries+ " tries to guess that.");
                System.out.println("\nPlay again? Y/N");
                playChoice = input.next().toUpperCase();

                if (playChoice.equals("Y")){ //Let's the player play again
                    tries = 0; // Resets the number of tries for the new round
                } //if
                else{ //Exits the while loop if the player doesn't want to play
                    playAgain++;
                } //else
            } // if
        } // while
        System.out.println("Alright, then. So long and thanks for all the fish!");
    } // main
} // CGuess (class)
