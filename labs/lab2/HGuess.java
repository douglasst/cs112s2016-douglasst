import java.util.Random;
import java.util.Scanner;

public class HGuess{
    public static void main(String[] args){
        int guess, answer, playAgain, tries;
        String answer2;

        Scanner input = new Scanner(System.in);

        playAgain = 0;
        tries = 0;
        while (playAgain < 1){ // loop containing the game
            if (playAgain == 0){ //allows the user to continually play

                System.out.println("I'm thinking of a number between 1-100. Guess what it is!");
                guess = input.nextInt();

                Random randy = new Random();
                answer = randy.nextInt(100)+1; // Random number generator lets the computer pick a number

                tries++;
                while (guess != answer ){ // lets the user guess
                    if (guess < answer){
                        System.out.println("That's too low! Guess again!");
                        guess = input.nextInt();
                        tries++;
                    } // if
                    else if (guess > answer){
                        System.out.println("That's too high! Guess again!");
                        guess = input.nextInt();
                        tries++;
                    } // else if
                } //while

                System.out.println("You got it! The number I was thinking of was " +answer+ ". \nIt took you " +tries+ " tries.");
                System.out.println("\n Play again? Y/N");
                answer2 = input.next().toUpperCase();

                if (answer2.equals("Y")){ //allows replayability
                    tries = 0;
                } //else
                else {
                    playAgain++; //exits the overarching while loop
                } //else
            } //if
        } //while
        System.out.println("Alright, so long and thanks for all the fish!");
    } // main
} // HGuess (class)
