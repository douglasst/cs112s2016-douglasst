import java.util.Scanner;
import java.io.*;

public class Authenticate{
    public static void main(String[] args) throws FileNotFoundException{
        File file = new File("passwd.txt");
        Scanner scan = new Scanner(file);


        HashTable hash = new HashTable();

        while (scan.hasNextLine()){
            String input = scan.nextLine();
            String User = input.substring(0,input.indexOf(' '));
            String Pass = input.substring(input.indexOf(' ')+1,input.length());
            hash.put(User, Pass);
        }

        hash.printAll();

        Scanner userInput = new Scanner(System.in);
        System.out.println("\n\nPlease enter your username. ");
        String username = userInput.next();
        System.out.println("Please enter your password. ");
        String password = userInput.next();

        int tries = 0;
        if (!hash.get(username).equals(password)){
            while(!hash.get(username).equals(password) && tries < 4){
                tries++;

                System.out.println("\nInvalid combination. Tries left: " + (5-tries));
                System.out.println("Please enter your username. ");
                username = userInput.next();
                System.out.println("Please enter your password. ");
                password = userInput.next();
            }
            if (hash.get(username) == password){
                 System.out.println("Login successful.");
            } else{
                System.out.println("Login failed. You have exhausted all of your attempts.");
            }
        }
        System.out.println("\nLogin successful.\n");
    } // main method
} // Authenticate (class)
