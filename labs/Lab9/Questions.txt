Questions:
    1. A checked exception is something that happens within a program
        that isn't fatal, but rather quite common so the program anticipates
        and looks for it (FileNotFoundException). A runtime exception is
        something unexpected that happens while the program is running that
        it does NOT prepare for (ArrayIndexOutOfBoundsException). I feel
        like the checked exception is "better" than the runtime exception,
        because a runtime exception is more likely to crash your program.

    2. Each chain should contain 25 nodes, on average. The smallest possible
    chain length would be 0, if no value hashed to that index, and the worst
    biggest possible chain length would be 5000, if every value hashed to that
    index.

    3.  20
        94 - 29 - 16
        12
        -
        -
        13
        -
        44 - 5
        -
        88 - 23
        -
        -
        11
