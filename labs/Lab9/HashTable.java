public class HashTable {
    private String keys[];
    private String values[];

    public HashTable() {
        values = new String[11];
        keys = new String[11];
    } // HashTable (constructor)

    public void put(String k, String v) {
        int index = hash(k);
        if (keys[index] == null) {
            keys[index] = k;
            values[index] = v;
        } else {
            // In the case that the index that the key hashes to isn't empty
            while (keys[index] != null) {
                index++;
                // To loop back to the beginning
                if (index == 11) {
                    index = 0;
                }
            }
            keys[index] = k;
            values[index] = v;
        }

    } // put()

    public String get(String k) {
        int index = hash(k);

        if (keys[index].equals(k)){
            return values[index];
        } else {
            // Iterating to find the key
            while (!keys[index].equals(k)) {

                // If the key doesn't exist
                if (keys[index] == null){
                    return null;
                }

                index++;
                // To loop back to the beginning
                if (index == 11) {
                    index = 0;
                }
            }
            return values[index];
        }
    } // get()

        public int hash(String k) {
            int sum = 0;
            for (int i = 0; i<k.length(); i++) {
                char c = k.charAt(i);
                int ascii = (int) c;

                sum += ascii;
            }

            int index = sum%11;

            return index;
        } // hash()

        public void printAll() {
            System.out.println("\nKeys: ");
            for (int i = 0; i< 11; i++) {
                System.out.print(keys[i] + ", ");
            }

            System.out.println("");

            System.out.println("\nValues: ");
            for (int j = 0; j < 11; j++) {
                 System.out.print(values[j] + ", ");
            }

        } // printAll()

    } // HashTable (class)
