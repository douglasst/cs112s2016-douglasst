public class Square extends Rectangle{

    public Square(double w){
        super(w,w);
    } // Constructor

    public void calculateArea(){
        area = width * width;
    } // calculateArea

    public void calculatePerimeter(){
        perimeter = width * 4;
    } // calculatePerimeter


} // Square (class)
