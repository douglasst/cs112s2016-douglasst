public class Trapezoid extends Quadrilateral{
    private double otherBase, side1, side2; //Variables

    public Trapezoid(double w, double h, double o, double a, double b){
        width = w;
        height = h;
        side1 = a;
        side2 = b;
        otherBase = o;
    } // Constructor

    public void calculateArea(){
        area = height * (width + otherBase)/2;
    } // calculateArea

    public void calculatePerimeter(){
        perimeter = width + side1 + side2 + otherBase;
    } // calculatePerimeter

} // Trapezoid (class)
