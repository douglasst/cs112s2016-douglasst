public class Oval extends Round{
    private double major, minor; //Variables

    public Oval(double a, double i){
        major = a;
        minor = i;

    } // Constructor

    public void calculateArea(){
        area = major * minor * pi;
    } // calculateArea

    public void calculatePerimeter(){
        major = major * major; //Just removing a step for calculating perimeter
        minor = minor * minor;
        perimeter = (2*pi)*(Math.sqrt((major + minor)/2));
    } // calculatePerimeter

} // Oval (class)
