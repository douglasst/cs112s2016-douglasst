public class Quadrilateral extends Shape{
    protected double width, height; //Variables

    public void setWidth(double w){
        width = w;

    } // setWidth

    public void setHeight(double h){
        height = h;

    } // setHeight

    public double getWidth(){
        return width;
    } // getWidth

    public double getHeight(){
        return height;
    } // getHeight
} // Quadrilateral (class)
