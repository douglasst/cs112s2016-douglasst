public class Rectangle extends Quadrilateral{
    public Rectangle(double w, double h){
        width = w;
        height = h;
    } // Constructor

     public void calculateArea(){
        area = width * height;
    } // calculateArea

    public void calculatePerimeter(){

        perimeter = width * 2 + height * 2;
    } // calculatePerimeter

} // Rectangle (class)
