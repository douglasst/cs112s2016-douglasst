public class Circle extends Round{
    private double radius; //Variable

    public Circle(double r){
        radius = r;

    } // Constructor

    public void calculateArea(){
        area = pi * radius * radius;
    } // calculateArea

    public void calculatePerimeter(){
        perimeter = 2 * pi * radius;
    } // calculatePerimeter
} // Circle (class)
