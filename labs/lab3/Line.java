public class Line extends Shape{
    private double x1, y1, x2, y2; // Initializing variables

    public Line(double a, double b, double c, double d){
        x1 = a;
        y1 = b;
        x2 = c;
        y2 = d;

    } // Constructor

    public void calculateArea(){
         area = 0;
    } //calculateArea()

    public void calculatePerimeter(){
        perimeter = 2 * (Math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)));
    } // calculatePerimeter

} // Line (class)
