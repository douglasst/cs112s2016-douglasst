import java.util.LinkedList;

public class LinkedArraysTester {

    public static void main(String[] args) {

        long startAdd1, startAdd2, startAdd3, startGet1, startGet2, startGet3,
             endAdd1, endAdd2, endAdd3, endGet1, endGet2, endGet3, totalAdd1,
             totalAdd2, totalAdd3, totalGet1, totalGet2, totalGet3;

        int dopeArray[] = new int[100000];
        LinkedList<Integer> dopeList = new LinkedList<Integer>();
        LL dopeLL = new LL();

        // Array
        startAdd1 = System.nanoTime();
        for (int i = 0; i<100000; i++) {
            dopeArray[i] = i;
        } // for
        endAdd1 = System.nanoTime();
        totalAdd1 = endAdd1 - startAdd1;

        // LinkedList
        startAdd2 = System.nanoTime();
        for (int i = 0; i<100000; i++) {
            dopeList.add(i);
        } // for
        endAdd2 = System.nanoTime();
        totalAdd2 = endAdd2 - startAdd2;

        // LL
        startAdd3 = System.nanoTime();
        for (int i = 0; i<100000; i++) {
            dopeLL.add(i);
        } // for
        endAdd3 = System.nanoTime();
        totalAdd3 = endAdd3 - startAdd3;

        System.out.println("Addition to an Array Runtime: " + totalAdd1 + " nanoseconds");
        System.out.println("Addition to a LinkedList Runtime: " + totalAdd2 + " nanoseconds");
        System.out.println("Addition to LL Runtime: " + totalAdd3 + " nanoseconds");

        // Array
        startGet1 = System.nanoTime();
        for (int i = 0; i<100000; i++) {
            int hey = dopeArray[i];
        } // for
        endGet1 = System.nanoTime();
        totalGet1 = endGet1 - startGet1;

        // LinkedList
        startGet2 = System.nanoTime();
        for (int i = 0; i<100000; i++) {
            int you = dopeList.get(i);
        } // for
        endGet2 = System.nanoTime();
        totalGet2 = endGet2 - startGet2;

        // LL
        startGet3 = System.nanoTime();
        for (int i = 0; i<100000; i++) {
            int guys = dopeLL.get(i);
        } // for
        endGet3 = System.nanoTime();
        totalGet3 = endGet3 - startGet3;

        System.out.println("\nRetrieval from an Array Runtime: " + totalGet1 + " nanoseconds");
        System.out.println("Retrieval from a LinkedList Runtime: " + totalGet2 + " nanoseconds");
        System.out.println("Retrieval from LL Runtime: " + totalGet3 + " nanoseconds");

    } // main

} // LinkedArraysTester (class)
