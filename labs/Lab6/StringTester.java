public class StringTester {

    public static void main(String[] args) {

        long startTime1, startTime2, endTime1, endTime2, total1, total2;

        String stringA = new String();
        StringBuilder stringB = new StringBuilder();

        startTime1 = System.nanoTime();
        for (int i = 0; i<100000; i++) {
            stringA = stringA + "A";
        }
        endTime1 = System.nanoTime();
        total1 = endTime1 - startTime1;

        startTime2 = System.nanoTime();
        for (int j = 0; j<100000; j++) {
            stringB.append("A");
        }
        endTime2 = System.nanoTime();
        total2 = endTime2 - startTime2;

        System.out.println("Concatenation Runtime: " + total1 + " nanoseconds");
        System.out.println("String Builder Runtime: " + total2 + " nanoseconds");

    } // main
} // StringTester (class)
