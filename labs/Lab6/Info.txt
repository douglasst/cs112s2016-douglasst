Name: Tara Douglass                 Assignment #: 6
Program Due Date: 4/9/16
Handed in Date:  4/9/16
Source code file name(s):
    LinkedArraysTester.java, LL.java, StringTester.java

Compiled (.jar or .class) file name(s):
    LinkedArraysTester.class, LL.class, LL$Node.class, StringTester.class

Does your program compile without error?: Yes

Does your program run without error?: Yes

Additional comments (including problems and extra credit):
    Everything went great!
