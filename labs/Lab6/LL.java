public class LL{

    private class Node {
        protected Node next;
        protected int data;

        public Node(){
            next = null;
            data = 0;
        } // Node (constructor)

        public Node(int d, Node n){
            data = d;
            next = n;
        } // Node (constructor)
    } // Node (class)

    private int size;
    private Node head, tail;

    public LL() {
        head = null;
        tail = null;
        size = 0;
    } // LL (constructor)

    public void add(int d){
        Node newNode = new Node(d, null); // create Node, add data

        if (size == 0){
            head = newNode;
            tail = newNode; // update tail reference
            size ++;
            return;
        } // if

        tail.next = newNode; // make tail point to new node
        tail = newNode; // update tail reference
        size++;
    } // add

    public void add(int index, int d){
        // 4 Cases: Add head, tail, first node, middle

        if (index > size || index < 0){
            System.out.println("INVALID INDEX - ADD");
            return;
        } // if

        // add tail
        if (index == size){
            add(d);
        } // if

        //add first node
        else if (size == 0){
            add(d);
        }

        //add head
        else if (index == 0){
            Node newHead = new Node(d, head);
            head = newHead;
            size ++;
        }

        //add middle
        else{
            Node currentLocation, previousLocation;
            currentLocation = head.next;
            previousLocation = head;
            int counter = 1;

            while (counter < index){
                 currentLocation = currentLocation.next;
                 previousLocation = previousLocation.next;
                 counter++;
            } // while

            Node newNode = new Node(d, currentLocation);
            previousLocation.next = newNode;
            size ++;
        } // else

    } // add

    public int get(int index){
        Node currentPosition = head;
        int counter = 0;

        // check to make sure index < size
        if (index >= size || index < 0){
            System.out.println("INVALID INDEX - GET");
            return -1;
        } // if

        while (counter < index){
            currentPosition = currentPosition.next;
            counter++;
        } //while

        return currentPosition.data;
    } // get

    public void remove(int index){
        // 4 cases: remove head, remove tail, remove single node, regular
        if (index >= size || index < 0){
            System.out.println("INVALID INDEX - REMOVE");
            return;
        } // if

        // removing head
        if (index == 0){
            head = head.next;

            // remove single node
            if (size == 1){
                tail = null;
            } // if
        } // if
        else {
            // removing a regular node
            Node currentLocation, previousLocation;
            currentLocation = head.next;
            previousLocation = head;
            int counter = 1;

            while (counter < index){
                 currentLocation = currentLocation.next;
                 previousLocation = previousLocation.next;
                 counter++;
            } // while

            previousLocation.next = currentLocation.next;

            // removing tail
            if (index == size - 1){
                 tail = previousLocation;
            } // if
        } // else
        size --;
    } // remove

    public int size(){
        return size;
    } // size
} // LL (class)
