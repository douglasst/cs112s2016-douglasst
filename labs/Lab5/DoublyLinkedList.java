public class DoublyLinkedList{

    private static class Node {

        protected Node next, previous;
        protected int data;

        public Node(){
            next = null;
            data = 0;
        } // Node (constructor)

        public Node(int d, Node n, Node p){
            data = d;
            next = n;
            previous = p;
        } // Node (constructor)
    } // Node (class)

    private int size;
    private Node head, tail;

    public DoublyLinkedList() {
        head = null;
        tail = null;
        size = 0;
    } // DoublyLinkedList (constructor)
	public int size() {
		return size;
	} //size

	public boolean isEmpty() {
		if (size == 0) {
			return true;
		} else {
			return false;
		} //if-else
	} //isEmpty

	public void add(int newInt) {
        //Adds a node to the end of the list (next value is null)
        Node newNode = new Node(newInt, null, tail);

        //Adds an initial node
        if (size == 0){
            head = newNode;
            tail = newNode;
            size ++;
        }
        else{
        //The node after the tail is the new node
        tail.next = newNode;
        //The node before the new node is the tail
        newNode.previous = tail;
        //The new tail is the new node
        tail = newNode;
        size ++;
	    }
    } //add

	public int get(int index) {
		Node currentPosition = head;
		int counter = 0;

		// check to make sure the index is less than size
		if (index >= size || index < 0) {
			System.out.println("Hey you gave me bad input -- get");
			return -1;
		} //if

		while (counter < index/* && index >= 0 && index < size*/) {
			currentPosition = currentPosition.next;
			counter++;
		} //while

		return currentPosition.data;
	} //get

	public int getFromEnd(int index) {
        //The get function from the other end
        Node currentPosition = tail;
		int counter = 0;

		// check to make sure the index is less than size
		if (index >= size || index < 0) {
			System.out.println("Hey you gave me bad input -- get");
			return -1;
		} //if

		while (counter < index/* && index >= 0 && index < size*/) {
            currentPosition = currentPosition.previous;
			counter++;
		} //while

        return currentPosition.data;
	} //getFromEnd
	public void remove(int index) {
        //Takes care of invalid input
        if (index >= size || index < 0){
            System.out.println("INVALID INDEX - REMOVE");
            return;
        } // if
        Node currentLocation, previousLocation;

        // removing head
        if (index == 0){
            currentLocation = head.next;
            head = currentLocation;

            // remove single node
            if (size == 1){
                tail = null;
            } // if
        } // if
        else {
            // removing a regular node
            currentLocation = head.next;
            previousLocation = head;
            int counter = 1;

            while (counter < index){
                 currentLocation = currentLocation.next;
                 previousLocation = previousLocation.next;
                 currentLocation.previous = previousLocation;
                 previousLocation.next = currentLocation;
                 counter++;
            } // while

            currentLocation = currentLocation.next;
            previousLocation.next = currentLocation;
            currentLocation.previous = previousLocation;

            // removing tail
            if (index == size - 1){
                 tail = previousLocation;
            } // if
        } // else
        size --;

	} //remove

	public boolean testPreviousLinks() {
		Node current = tail;
		int count = 1;
		while (current.previous != null) {
			current = current.previous;
			count++;
		} //while
		return ((current == head) && (count == size));
	} //testPreviousLinks

} // DoublyLinkedList (class)
